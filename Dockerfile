# 
# Extended from insighttoolkit/itk-base Dockerfile
#
FROM centos:latest
MAINTAINER Tahsin Kurc 

RUN yum -y update && yum -y install \
	build-essential \
	valgrind \
	valgrind-devel \
	curl \
	git \
	ncurses-devel \
	vim \
	gtk2-devel \
	pkgconfig \
	python-devel \
	numpy \
	tbb \
	tbb-devel \
	libuuid \
	libuuid-devel \
	jasper-devel \
	zip \
	unzip \
	epel-release \
	wget

RUN yum -y group install "Development Tools"

RUN yum -y install libxml2-devel 
RUN yum -y install sqlite-devel 
RUN yum -y install cmake*

# Install libraries
WORKDIR /tmp/

# Install libraries needed for openslide 3.4.1
RUN curl -O -J -L http://downloads.sourceforge.net/lcms/lcms2-2.7.tar.gz && \
	tar -xzvf lcms2-2.7.tar.gz && \
	cd lcms2-2.7 && ./configure && \
	make -j4 && make install && \
	cd .. && rm -rf lcms2-2.7* 

RUN curl -O -J -L http://downloads.sourceforge.net/libpng/libpng-1.6.22.tar.xz && \
	tar -xvf libpng-1.6.22.tar.xz && \
	cd libpng-1.6.22 && ./configure && \
	make -j4 && make install && \
	cd .. && rm -rf libpng-1.6.22*

RUN curl -O -J -L http://download.osgeo.org/libtiff/tiff-4.0.6.tar.gz && \
	tar -xzvf tiff-4.0.6.tar.gz && \
	cd tiff-4.0.6 && ./configure && \
	make -j4 && make install && \
	cd .. && rm -rf tiff-4.0.6* 

RUN curl -O -J -L http://downloads.sourceforge.net/openjpeg.mirror/openjpeg-2.1.0.tar.gz && \
	tar -xzvf openjpeg-2.1.0.tar.gz && \
	cd openjpeg-2.1.0 && mkdir build && \
	cd build && cmake3 ../ && \
	make -j4 && make install && \
	cd ../.. && rm -rf openjpeg-2.1.0*

RUN yum -y install cairo-devel gdk-pixbuf2-devel

ENV PKG_CONFIG_PATH=/usr/local/lib/pkgconfig/

# Install openslide 3.4.1
RUN curl -O -J -L https://github.com/openslide/openslide/releases/download/v3.4.1/openslide-3.4.1.tar.gz && \
	tar -xzvf openslide-3.4.1.tar.gz && \
	cd openslide-3.4.1 && \
	./configure && \
	make -j4 && \
	make install && \
	cd .. && \
	rm -rf openslide-3.4.1*

# Install OpenCV 2.4.11
RUN curl -O -J -L https://github.com/Itseez/opencv/archive/2.4.11.zip && \
	unzip opencv-2.4.11.zip && \
	mkdir /tmp/opencv-build && \
	cd /tmp/opencv-build && \
	cmake -D CMAKE_BUILD_TYPE=RELEASE -D BUILD_TESTS=OFF -D CMAKE_INSTALL_PREFIX=/usr/local ../opencv-2.4.11 && \
	make -j4 && \
	make install && \
	cd /tmp && \
	rm -rf opencv-build && \
	rm -rf opencv-2.4.11* 

# Install ITK 4.7.2
# COPY InsightToolkit-4.7.2.tar.gz /tmp/.
# RUN tar -xzvf InsightToolkit-4.7.2.tar.gz && \
RUN curl -O -J -L http://sourceforge.net/projects/itk/files/itk/4.9/InsightToolkit-4.9.1.tar.gz/download && \
	tar -xzvf InsightToolkit-4.9.1.tar.gz && \
	mkdir -p /tmp/itk-build && \
	cd /tmp/itk-build && \
	cmake -D BUILD_EXAMPLES:BOOL=OFF -D BUILD_TESTING:BOOL=OFF -D BUILD_TESTS:BOOL=OFF -D Module_ITKVideoBridgeOpenCV:BOOL=ON -D Module_ITKReview:BOOL=ON ../InsightToolkit-4.9.1 && \
	make -j4 && \
	make install && \
	cd /tmp && \
	rm -rf itk-build && \
	rm -rf InsightToolkit-4.9.1*

# Install analysis codes
RUN git clone https://github.com/SBU-BMI/pathomics_analysis.git && \
	cd pathomics_analysis && \
	cd nucleusSegmentation && \
	mkdir build && \
	cd build && \
	cmake ../src && \
	make -j4 && \
	cp app/main* /usr/local/bin/. && \
	cp ../script/mainAggregateFeatures.py  /usr/local/bin/. && \
	cd /tmp  

# Add data folder
RUN mkdir -p /data/input
RUN mkdir -p /data/output

CMD ["/bin/bash"]
